#!/usr/bin/env python
# coding: utf-8

""" Функции локализации представления данных.
"""

from errors import ArgumentError
from datetime import datetime

from babel import numbers, dates

from ivideon.utils import unspect
from ivideon.utils.time_tools import get_canonical_timezone


# Контекст генерации сообщения
_context = None


def set_context(new_context):
    """ Установить контекст генерации сообщений
    """
    global _context
    _context = new_context


def _c(val, currency=None):
    """ Локализация денежной суммы.
    """
    currency = currency or _context['args'].get('currency')
    if currency is None:
        raise ArgumentError('_c localization function requires "currency" argument', details={'arg': 'currency'})

    return numbers.format_currency(val, currency, locale=_context["locale"])


def _n(val):
    """ Локализация числа.
    """
    return numbers.format_number(val, locale=_context["locale"])


def _p(percent):
    """ Локализация процентов.
    """
    return numbers.format_percent(percent, locale=_context["locale"])


def _d(date, format='medium'):
    """ Локализация даты.
    """
    date = datetime.fromtimestamp(date)
    return dates.format_date(date, format=format, locale=_context["locale"])


def _t(time, format='medium'):
    """ Локализация времени.
    """
    time = datetime.fromtimestamp(time)
    return dates.format_time(time, format=format, locale=_context["locale"])


def _dt(dtime, format='medium', timezone=None):
    """ Локализация даты и времени.
    """
    dtime = datetime.utcfromtimestamp(dtime)
    timezone = get_canonical_timezone(timezone or _context["timezone"])
    return dates.format_datetime(
        dtime, format=format,
        locale=_context["locale"],
        tzinfo=timezone
    )


functions = {name: f for name, f in globals().iteritems() if unspect.isroutine(f)}
