# coding: utf-8

""" Дополнительные функции, доступные в шаблонах.
"""

import os
import re
from ivideon.utils import unspect

import urlparse
from urllib import urlencode


def _img(url):
    """ Подготовка изображения для вставки в письмо.

        @todo
            Добавить возможность встраивания картинки в письмо.

        >>> _img("ya.ru")
        'https://ya.ru'

        >>> _img("http://ya.ru")
        'http://ya.ru'
    """
    if not url.startswith('http'):
        url = 'https://{}'.format(url)

    return url


SUBJECT_RE = re.compile("<br ?/?>|&nbsp;")


def _subject(line):
    """ Удаление простейших HTML конструкций для формирования темы письма.
        >>> _subject("Hello&nbsp;username!<br>How are you?<br/>How is your day?<br />")
        'Hello username! How are you? How is your day? '
    """
    return SUBJECT_RE.sub(" ", line)


def _strip(string, chars=None):
    """ Удаление символов с обоих сторон строки.
        >>> _strip(' \t asd \t')
        'asd'
        >>> _strip('asd xabbbaa', 'ab')
        'sd x'
    """
    return string.strip(chars)


def _lstrip(string, chars=None):
    """ Удаление символов с начала строки.
        >>> _lstrip(' \t asd')
        'asd'
        >>> _lstrip('asd xabbbaa', 'ab')
        'sd xabbbaa'
    """
    return string.lstrip(chars)


def _rstrip(string, chars=None):
    """ Удаление символов с конца строки.
        >>> _rstrip('asd \t')
        'asd'
        >>> _rstrip('asd xabbbaa', 'ab')
        'asd x'
    """
    return string.rstrip(chars)


def _unpack_query(query):
    """ В словаре `query`, полученном в результате выполнения функции
        urlparse.parse_qs, распаковывает значения, упакованные в список.
    """
    result = {}
    for k, v in query.iteritems():
        if isinstance(v, list) and v:
            v = v[0]
        result[k] = v
    return result


def _update_qs(url, params):
    """ Добавляет параметры в строку запроса.
        >>> params = {'lang':'en','q':'python'}
        >>> url = "http://yandex.com/search?q=ru%20by"
        >>> _update_qs(url, params)
        'http://yandex.com/search?q=python&lang=en'
    """
    url_parts = list(urlparse.urlparse(url))
    query = dict(urlparse.parse_qs(url_parts[4]))
    query = _unpack_query(query)
    query.update(params)
    url_parts[4] = urlencode(query)
    return urlparse.urlunparse(url_parts)


def _update_dict(dict_a, dict_b):
    """ Формирует словарь со всеми ключами из a и b.
        В случае коллизий, используется значение из b.
        >>> _update_dict({'x': 1, 'y': 2}, {'x': 3, 'z': 4})
        {'y': 2, 'x': 3, 'z': 4}
    """
    return dict(dict_a, **dict_b)


def _extend_dict(dict_a, dict_b):
    """ Формирует словарь со всеми ключами из a и b.
        В случае коллизий, используется значение из a.
        >>> _extend_dict({'x': 1, 'y': 2}, {'x': 3, 'z': 4})
        {'y': 2, 'x': 1, 'z': 4}
    """
    return dict(dict_b, **dict_a)


def _filename(path):
    """ Возвращает имя файла из адреса файла.
        >>> _filename("/x/y/z.txt")
        'z.txt'
    """
    return os.path.basename(path)


def _dirname(path):
    """ Возвращает адрес директории из адреса файла.
        >>> _dirname("/x/y/z.txt")
        '/x/y'
        >>> _dirname("x/y/z.txt")
        'x/y'
    """
    return os.path.dirname(path)


#-------------------------------------------------------------------------------

functions = {name: f for name, f in globals().iteritems() if unspect.isroutine(f)}


if __name__ == "__main__":
    import doctest
    doctest.testmod()
