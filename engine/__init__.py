#!/usr/bin/env python
# coding: utf-8

""" Template engine - модуль для генерации сообщений по шаблонам.
    Перед началом использования, нужно вызвать метод configure.
"""

from engine import render, configure
from translation import compile_translations, clear_cache, message_catalog
import errors
