#!/usr/bin/env python
# coding: utf-8

""" Главный модуль template engine.
"""

import re
import logging

import pynliner
import jinja2
from jinja2.sandbox import SandboxedEnvironment
from jinja2 import FileSystemLoader
from functools import partial

from ivideon.utils import settings

import localization
import translation
from errors import EngineError, ArgumentError, TemplateError
import utils


TEMPLATE_DIR = None
DO_CSS_INLINE = settings.get('do_css_inline', True)

_loader = None
_env = None

# Контекст генерации сообщения:
_context = {
    "template": None,       # шаблон
    "args": None,           # аргументы шаблона
    'lang': None,           # язык
    'locale': None,         # локаль
    'timezone': None,       # таймзона
    'have_timezone': None   # наличие таймзоны пользователя
}

localization.set_context(_context)
translation.set_context(_context)


# Патч для исправления ошибки работы с неюникодовыми символами в
# /usr/local/lib/python2.7/dist-packages/pynliner/__init__.py 211
def pynliner_patch(self):
    self.output = unicode(self.soup.__str__('utf-8'), 'utf-8')
    return self.output

pynliner.Pynliner._get_output = pynliner_patch


def dictsort(value, case_sensitive=False, by='key', attribute=None, reverse=False):
    """ Расширение builtin фильтра dictsort для возможности сортировки по атрибуту.
    """
    return sorted(
        value.items(),
        key=lambda i: i[1][attribute] if attribute else i[0] if by == 'key' else i[1],
        reverse=reverse
    )


def configure(template_dir, translation_dir):
    """ Сконфигурировать шаблонизатор.
    """
    global _env
    global _loader
    global TEMPLATE_DIR

    TEMPLATE_DIR = template_dir
    translation.MC_DIR = translation_dir

    logging.info("Template search path: {}".format(TEMPLATE_DIR))
    logging.info("Translation search path: {}".format(translation.MC_DIR))

    _loader = FileSystemLoader(TEMPLATE_DIR)
    _env = SandboxedEnvironment(
        loader=_loader,
        extensions=['jinja2.ext.i18n']
    )
    _env.filters['dictsort'] = dictsort


def render(org, path, lang, locale, timezone, targs=None, **kwargs):
    """ Сформировать сообщение по шаблону.
        Аргументы шаблона можно передавать через словарь targs и kwargs.
    """
    if targs is None:
        targs = kwargs
    else:
        targs.update(kwargs)

    lang = lang or 'en'
    locale = locale or 'en'
    have_timezone = bool(timezone)
    timezone = timezone or 'UTC'

    # Внедрение дополнительных функций и переменных, доступных в шаблонах.
    targs.update(_=partial(translation.gettext, org))
    targs.update(localization.functions)
    targs.update(utils.functions)
    targs.update(lang=lang, locale=locale, timezone=timezone)
    targs.update(template_path=path)

    _context['template'] = path
    _context['args'] = targs
    _context['lang'] = lang
    _context['locale'] = locale
    _context['timezone'] = timezone
    _context['have_timezone'] = have_timezone

    # Генерируем html по шаблону
    try:
        t = _env.get_template(path)
        rendered = t.render(**targs)
    except EngineError:
        raise
    except Exception as e:
        details = None
        if isinstance(e, jinja2.TemplateError):
            if isinstance(e, jinja2.UndefinedError):
                # Ошибка из-за нехватки аргументов
                m = re.match("'([^']*)' is undefined", e.message) or\
                    re.match("^.*has no attribute '([^']*)'", e.message)
                raise ArgumentError(e.message, details={'arg': m.groups()[0] if m else 'unknown'})
            else:
                # Ошибка шаблона или привязки
                details = {
                    'class': e.__class__.__name__,
                    'info': e.__dict__,
                }
                logging.error("TemplateError: {}".format(details), exc_info=True)
                raise TemplateError(e.message, details=details)
        else:
            # Если не TemplateError, то это либо внутренняя ошибка, либо ошибка выполнения шаблона.
            logging.error(u'Template engine error: ({}, {}, {})'.format(path, lang, locale), exc_info=True)
            details = {
                'class': 'TemplateRuntimeError',
                'info': {},
            }
            raise TemplateError(e.message, details=details)

    result = rendered

    if DO_CSS_INLINE:
        # Применяем css inliner
        try:
            result = pynliner.fromString(rendered)
        except Exception as e:
            logging.error('CSS inliner failed. Path= {}', path, exc_info=True)
            raise TemplateError('CSS inliner failed', details={'class': 'CSSInline', 'info': {'message': unicode(e)}})

    return result
