#!/usr/bin/env python
# coding: utf-8

""" Тестер шаблонизатора Notifier.
    Производит рендер тестового шаблона.
    Может рендерить один шаблон, либо работать как сервер рендеринга.

    Пример команды генерации:
        python -m ivideon.notifier.engine.etest render -c ivideon/notifier-templates -t default/smm-invite-friend-complete/email/html.jinja2 -fs '{"info": {"currency": "RUR", "amount": 50000}, "uri": {"my_ivideon": "https://www.ivideon.com/my"}, "partner": {"id": "ivideon"}}'

    Команда запуска сервера:
        python -m ivideon.notifier.engine.etest serve -c ivideon/notifier-templates

    Пример запроса к серверу:
        curl -X POST localhost:8008/messages/render -d @msg

        Где файл msg содержит:
            {
                "args": {
                    "partner": {
                        "id": "ivideon",
                        "name": "Ivideon"
                    },
                    "uri": {
                        "my_ivideon": "https://www.ivideon.com/my"
                    },
                    "user": {
                        "partner_data": {
                            "id": "100000407004"
                        },
                        "email": "chira412822yandex.ru"
                    },
                    "mail": {
                        "support": "support@ivideon.com"
                    },
                    "get_ivideon_button": "asd"
                },
                "path": "default/user-signup/email/html.jinja2",
                "lang": "ru",
                "locale": "ru",
                "config_path": "ivideon/notifier-templates"
            }
"""

from __future__ import print_function

import os
from sys import stderr
import json
import argparse
import logging
from time import time
import datetime
import traceback
from StringIO import StringIO

from watchdog.utils.dirsnapshot import DirectorySnapshotDiff, DirectorySnapshot

import __init__ as ng


logging.basicConfig(level=logging.INFO)

last_e = None

default_args = {
    "user_name": 'Vasia',
    "user": {'name': 'Vasilii', 'family': 'Zaycev'},
    "amount": 5,
    "time": time(),
    "currency": 'USD',
    "cost": 5349.2587,
}



_last_config_path = None


def config_engine(config_path):
    global _last_config_path

    if config_path == _last_config_path:
        return

    _last_config_path = config_path
    template_dir = os.path.join(config_path, 'templates')
    mc_dir = os.path.join(config_path, 'translations')
    ng.configure(template_dir, mc_dir)


def engine_error_report(e):
    """ Сообщение об ошибке шаблонизатора.
    """
    errstream = StringIO()
    print("Test failed. EngineError:", type(e), file=errstream)
    print("Message:", e.message, file=errstream)
    if hasattr(e, 'details') and e.details is not None:
        e.details.get('info', {}).pop('source', None)
        print("Details:", json.dumps(e.details, indent=4), file=errstream)
        if e.details.get('class') == 'TemplateRuntimeError':
            tb = ''.join(traceback.format_tb(e.exc_info[2]))
            print("Traceback:", file=errstream)
            print(tb, file=errstream)

    report = errstream.getvalue()
    return report


def render(org, template, lang, locale, *args, **kwargs):
    """ Отрендерить сообщение.
    """
    global last_e
    try:
        result = ng.render(org, template, lang, locale, *args, **kwargs)
        result = result.encode('utf-8')
        return result

    except ng.errors.EngineError as e:
        last_e = e
        report = engine_error_report(e)
        raise Exception(report)

    except Exception as e:
        last_e = e
        raise Exception(u"Unknown error: {}".format(e.message))


def test():
    render(
        'ivideon',
        'common/email/user_register.html',
        'ru',
        'ru',
        default_args
    )


_types = [int, float]


def parse(val):
    for t in _types:
        try:
            return t(val)
        except:
            pass
    return val


def have_diff(prev_snapshot, new_snapshot):
    """ Есть ли разница между двумя состояниями директории.
    """
    diff = DirectorySnapshotDiff(prev_snapshot, new_snapshot)

    attrs = [
        "dirs_created",
        "dirs_modified",
        "files_created",
        "files_modified",
        "dirs_deleted",
        "dirs_moved",
        "files_deleted",
        "files_moved",
    ]
    for name in attrs:
        if [file for file in getattr(diff, name) if not file.endswith('.mo')]:
            return True

    return False


def run_server(args):
    """ Рендер сервер.
    """
    import tornado.ioloop
    import tornado.web
    import tornado.websocket
    from tornado.web import RequestHandler

    verbose = args.get('verbose')
    port = args.get('port')

    # Снимки директорий с переводами
    prev_tr_snapshots = {}

    class RenderHandler(RequestHandler):
        def post(self):
            """ Провести рендер сообщения.
            """
            try:
                msg = json.loads(self.request.body)
            except Exception as e:
                self.set_status(400)
                logging.warning(u'Malformed json. {}'.format(e.message))
                logging.warning(u"Body:\n{}".format(self.request.body))
                self.write(u'Malformed json. {}'.format(e.message))
                return

            config_path = msg.get('config_path')
            compile_tr = msg.get('compile')

            logging.info(u"Rendering message:\n{}".format(json.dumps(msg, indent=4)))

            if config_path is not None:
                config_engine(config_path)
            else:
                config_path = _last_config_path

            if compile_tr is None:
                mc_dir = os.path.join(config_path, 'translations')
                new_snapshot = DirectorySnapshot(mc_dir, True)
                prev_snapshot = prev_tr_snapshots.get(config_path)
                if not prev_snapshot or have_diff(prev_snapshot, new_snapshot):
                    prev_tr_snapshots[config_path] = new_snapshot
                    compile_tr = True

            if compile_tr:
                try:
                    ng.compile_translations()
                    ng.clear_cache()
                except Exception as e:
                    self.set_status(500)
                    self.write(u"Translation error: {}".format(e.message))
                    return

            error = None
            org = msg.get('org') or msg.get('args', {}).get('partner', {}).get('id') or 'ivideon'
            try:
                result = render(org, msg['path'], msg['lang'], msg['locale'], msg['args'])
            except KeyError as e:
                error = u"Missing request argument: {}".format(e.message)
            except Exception as e:
                error = u"Rendering error.\n{}".format(e.message)
                logging.error(error, exc_info=True)

            if error is not None:
                self.set_status(400)
                self.write(error)
            else:
                self.write(result)

    class ConfigReloadHandler(RequestHandler):
        def post(self):
            """ Перезагрузить конфиг.
            """
            config_path = self.get_argument('config_path', default=None)
            if config_path is not None:
                logging.info(u'Setting config path to: {}'.format(config_path))
                config_engine(config_path)

            try:
                logging.info(u'Compiling translations. path: {}'.format(ng.message_catalog()))
                ng.compile_translations()
                ng.clear_cache()
            except Exception as e:
                self.set_status(500)
                self.write(u"Translation error: {}".format(e.message))
                return

    def logger(handler):
        """ Логирование запросов, завершившихся с ошибкой.
        """
        request_time = 1000.0 * handler.request.request_time()
        if verbose:
            logging.info("%s %d %s %.2fms" % (datetime.datetime.now(), handler.get_status(), handler._request_summary(), request_time))

    application = tornado.web.Application(
        [
            (r"/messages/render", RenderHandler),
            (r"/config/reload", ConfigReloadHandler)
        ],
        log_function=logger
    )

    print("Staring render server on port:", port)
    application.listen(port)

    try:
        tornado.ioloop.IOLoop.instance().start()
    except KeyboardInterrupt:
        logging.info('Stopping server: Interrupted')
        exit()


def configure(config_path):
    """ Настроить шаблонизатор.
    """
    try:
        config_engine(config_path)
        ng.compile_translations()
        ng.clear_cache()
    except ng.errors.EngineError as e:
        report = engine_error_report(e)
        print(report, file=stderr)
        raise


def console_render(args):
    """ Консольный рендерер.
    """
    config_path = args['config']
    template = args['template']
    cargs = args['arg']
    argfile = args.get('argfile')
    argstr = args.get('argstr')
    lang = args['lang']
    locale = args['locale']
    outfile = args.get('out')
    verbose = args.get('verbose')
    org = args.get('org')

    try:
        configure(config_path)
    except:
        exit(1)

    targs = {}
    if args.get('defaultargs'):
        targs.update(default_args)

    # Считываем аргументы шаблона из файла
    if argfile:
        try:
            with open(argfile, 'r') as f:
                targs = json.load(f)
        except Exception as e:
            print("Can not read arg file. Exception:", e, file=stderr)
            exit(1)

    # Считываем из командной строки аргументы, упакованные в JSON.
    if argstr:
        try:
            targs = json.loads(argstr)
        except Exception as e:
            print("Can not parse arg string. Exception:", e, file=stderr)
            exit(1)

    # Считываем аргументы шаблона из коммандной строки
    for a in cargs:
        p = a.partition('=')
        if not p[1]:
            print("Invalid -a argument value:", a, file=stderr)
            print('Example: -a user_name=Vasilii', file=stderr)
            exit(1)
        targs[p[0]] = parse(p[2])

    if verbose:
        print("Template name:", template, file=stderr)
        print("Template arguments:", json.dumps(targs, indent=4), file=stderr)
    else:
        logging.disable(logging.info)

    try:
        result = render(org, template, lang, locale, targs)
    except Exception as e:
        print(e.message, file=stderr)
        exit(2)

    if outfile:
        try:
            with open(outfile, 'w') as f:
                f.write(result)
        except Exception as e:
            print("Can not write result. Exception:", e, file=stderr)
    else:
        print(result)


def run():
    parser = argparse.ArgumentParser(description='Notifier template engine test tool.')
    subs = parser.add_subparsers(dest='subparser_name', title='Subcommands')

    inf = 'Render template and exit.'
    render_parser = subs.add_parser('render', help=inf, description=inf)
    render_parser.add_argument('-c', '--config', required=True, help='Config directory path')
    render_parser.add_argument('-t', '--template', default='test.html', help='Template path (according to {config}/templates)')
    render_parser.add_argument('-p', '--org', default='ivideon', help='Organization')
    render_parser.add_argument('-lg', '--lang', default='ru', help='Message language')
    render_parser.add_argument('-lc', '--locale', default='ru', help='Message locale')
    render_parser.add_argument('-a', '--arg', nargs='*', default=[], help='Template argument')
    render_parser.add_argument('-f', '--argfile', required=False, help='File with template arguments in JSON format')
    render_parser.add_argument('-fs', '--argstr', required=False, help='String with template arguments in JSON format')
    render_parser.add_argument('-o', '--out', required=False, help='Output file')
    render_parser.add_argument('-d', '--defaultargs', action='store_const', const=True, help='Use default arguments')
    render_parser.add_argument('-v', '--verbose', action='store_const', const=True, help='Be more talkative')

    inf = 'Run template engine test tool as a rendering server.'
    serve_parser = subs.add_parser('serve', help=inf, description=inf)
    serve_parser.add_argument('-p', '--port', default='8008', help='Rendering server port')
    serve_parser.add_argument('-c', '--config', required=False, help='Initial config directory path')
    serve_parser.add_argument('-v', '--verbose', action='store_const', const=True, help='Be more talkative')

    args = vars(parser.parse_args())

    if args.get('subparser_name') == 'serve':
        config_path = args.get('config')
        if config_path:
            config_engine(config_path)

        run_server(args)
    else:
        console_render(args)


if __name__ == "__main__":
    run()
