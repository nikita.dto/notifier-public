#!/usr/bin/env python
# coding: utf-8

""" Ошибки шаблонизатора.
"""

import sys


class EngineError(Exception):
    """ Базовый класс для ошибок генератора сообщений.
    """
    def __init__(self, message='', details=None):
        Exception.__init__(self, message)
        self.details = details
        self.exc_info = sys.exc_info()


class ArgumentError(EngineError):
    """ Ошибка нехватки аргументов шаблона.
    """
    def __init__(self, *args, **kwargs):
        EngineError.__init__(self, *args, **kwargs)


class ValueError(EngineError):
    """ Ошибка значения аргумента.
    """
    def __init__(self, *args, **kwargs):
        EngineError.__init__(self, *args, **kwargs)


class TranslationError(EngineError):
    """ Ошибка доступа к локализационным словарям.
    """
    def __init__(self, *args, **kwargs):
        EngineError.__init__(self, *args, **kwargs)


class TemplateError(EngineError):
    """ Ошибка шаблона.
    """
    def __init__(self, *args, **kwargs):
        EngineError.__init__(self, *args, **kwargs)
