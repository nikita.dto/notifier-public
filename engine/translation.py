#!/usr/bin/env python
# coding: utf-8

""" Модуль интернационализации.
"""

import copy
import logging
import subprocess
import gettext as gt


from errors import TranslationError, ArgumentError


# Путь к каталогу сообщений
MC_DIR = None

# Контекст генерации сообщения
_context = None

# Символ, отделяющий в msgid домен и внутридоменный идентификатор перевода
DOMAIN_DELIMITER = '#'


def message_catalog():
    """ Возвращает каталог сообщений "по умолчанию"
    """
    return MC_DIR


def set_context(new_context):
    """ Установить контекст генерации сообщений
    """
    global _context
    _context = new_context


def cached(func):
    """ Кеширующий декоратор.
        !ACHTUNG! Только для функций с хешируемыми аргументами.
    """
    cache = {}

    def wrapped(*args):
        res = cache.get(args)
        if not res:
            res = func(*args)
            cache[args] = res
        return res
    wrapped.cache = cache
    return wrapped


@cached
def _get_translation(domain, lang):
    """ Получить GNUTranslation объект.
    """
    return gt.translation(domain, MC_DIR, [lang, 'en'], codeset='UTF-8')


def clear_cache():
    """ Очистить кеш переводов.
        !ACHTUNG! Для сброса кеша переводов используются недокументированные возможности,
        использование этого метода может привести к непредстазуемым последствиям.
    """
    _get_translation.cache.clear()
    try:
        gt._translations.clear()
    except:
        logging.error(
            "Unknown gettext module version. Can not clear translation cache.",
            exc_info=True
        )


def compile_translations(path=None):
    """ Скомпилировать файлы переводов po -> mo.
    """
    path = path or MC_DIR
    out = ''

    def error(exc_info):
        message = u"Can not compile translations. Path={}. Output={}.".format(path, out)
        logging.error(message, exc_info=exc_info)
        raise TranslationError(message, details={})

    try:
        out = subprocess.check_output(
            """for i in `find -name *.po`
               do
                   f=$(basename $i)
                   d=$(dirname $i)/LC_MESSAGES
                   mkdir -p $d
                   msgfmt $i -o "$d/${f%\.*}.mo"
               done
            """,
            shell=True,
            stderr=subprocess.STDOUT,
            cwd=path
        )
    except subprocess.CalledProcessError as e:
        out = e.output
        error(True)
    except Exception as e:
        error(True)

    if out:
        error(False)


def _gettext(domain, msgid, *args, **kwargs):
    """ Обёртка для gettext и ngettext.
        Если в kwargs есть n, работает как ngettext.
    """
    lang = _context['lang']
    template = _context['template']

    # Получаем объект перевода
    try:
        trn = _get_translation(domain, lang)
    except:
        message = 'Can not get translation for domain (domain={}, lang={})'.format(domain, lang)
        details = {'msgid': msgid, 'domain': domain, 'lang': lang, 'template': template}
        raise TranslationError(message, details=details)

    # Выбираем get_text или ngettext
    n = kwargs.get('n')
    if n is None:
        res = trn.gettext(msgid)
    else:
        if not isinstance(n, int):
            raise Exception(u"ngettext: n is not integer. n={}.".format(n))
        res = trn.ngettext(msgid, msgid, n)
    res = res.decode('utf-8')

    # Проверяем, был ли осуществлён перевод.
    if res == msgid:
        details = {'msgid': msgid, 'domain': domain, 'lang': lang, 'template': template, 'file': 'unknown'}
        raise TranslationError(
            'Translation not found for "{}" (maybe msgid == msgstr)'.format(msgid),
            details=details
        )

    # Производим форматирование переведённой строки, передавая в format все аргументы шаблона, kwargs и args.
    cp = copy.copy(_context['args'])
    cp.update(kwargs)
    try:
        res = res.format(*args, **cp)
    except KeyError as e:
        raise ArgumentError(
            u'Argument "{}" is not found for message: ("{}", "{}").'.format(e.message, msgid, res),
            details={'arg': e.message, 'msgid': msgid, 'domain': domain, 'msgstr': res, 'lang': lang}
        )
    except IndexError:
        raise ArgumentError(
            u'Not enough arguments for message: ("{}", "{}").'.format(msgid, res),
            details={'arg': None, 'msgid': msgid, 'domain': domain, 'msgstr': res, 'lang': lang}
        )

    return res


def gettext(org, msgid, *args, **kwargs):
    """ Обёртка над _gettext, обеспечивающая fallback поиска перевода
        в специализированном файле для организации.
        Порядок поиска перевода, если домен не указан явно в msgid:
            для искомого языка в домене org
            для английского языка в домене org
            для искомого языка в домене default
            для английского языка в домене default

        Порядок поиска перевода, если домен указан в msgid:
            для искомого языка в указанном домене
            для английского языка в указанном домене
    """
    errors = []
    domains = []

    if DOMAIN_DELIMITER in msgid:
        # Домен задан явно
        # Извлекаем домен из msgid
        p = msgid.partition(DOMAIN_DELIMITER)
        explicit_domain = p[0]
        msgid = p[2]
        domains = [explicit_domain]
    else:
        # Домен не задан явно - сначала проверяем наличие перевода в домене организации
        # если там нет, то в домене по умолчанию
        domains = [org, 'default']

    for domain in domains:
        try:
            return _gettext(domain, msgid, *args, **kwargs)
        except TranslationError as e:
            errors.append(e)
        except Exception as e:
            break

    for exc in errors:
        logging.warning('TranslationError. message: {}. details: {}.'.format(exc.message, exc.details))

    raise
