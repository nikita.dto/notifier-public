#!/usr/bin/env python
# -*- coding: utf-8 -*-

import logging
from datetime import datetime
from time import time

import json
import bson
import pymongo
import tornado.ioloop
import tornado.web
import tornado.websocket
from tornado.web import RequestHandler

from ivideon.utils import dict_tools, settings
from ivideon.utils.debug import listen_signals
from ivideon.utils.service import init_service
from ivideon.utils.handlers import status_route
from ivideon.utils.discovery_tools import ServiceType


from ..results import APIResult, APIError, APIException
from ..errors import Codes
from . import db


class BodyError(APIException):
    def __init__(self, message):
        APIException.__init__(self, Codes.Request_Bad, message)


def validate(msg):
    def check_fields(msg, fields):
        for f in fields:
            if f not in msg:
                raise BodyError('Missing field: {}'.format(f))

    check_fields(msg, ['channel', 'recipients'])
    f = 'recipients'
    if not isinstance(msg[f], list):
        raise BodyError(
            'Field "{}" is expected to be of type "{}"'.format(f, str(list))
        )

    if 'event' in msg:
        check_fields(msg, ['event', 'org', 'lang', 'locale', 'args'])

    if msg.get('aggregation_period'):
        check_fields(msg, ['aggregation_pipeline'])


def try_to_group(msg):
    """ Добавляет сообщение в группу, если группа уже существует.
    """
    aggregation_period = msg.get('aggregation_period')

    query = {}

    # Критерий группировки
    fields = (
        msg.get('group_by') or
        [
            'event'
        ]
    )
    group_by = {f: msg[f] for f in fields}
    query.update(group_by)

    # Группируем строго в пределах одной организации
    query['org'] = msg['org']

    # Группируем строго по получателям
    query['recipients'] = msg['recipients']

    # Добавляем только к группам, ожидающим своей очереди
    query['status'] = 'pending'

    # Учитываем ограничение периода агрегации
    query['queuing_time'] = {
        '$gte': datetime.fromtimestamp(time() - aggregation_period)
    }

    group = db.queue().messages.find_one_and_update(
        query,
        {
            '$push': {
                'args': msg['args']
            }
        },
        return_document=pymongo.ReturnDocument.AFTER
    )

    return group


def add_to_queue(msg):
    """ Добавление нового уведомления в очередь.
    """
    msg['status'] = 'pending'
    msg['queuing_time'] = datetime.now()
    msg['process_after'] = datetime.fromtimestamp(
        time() + msg.get('aggregation_period', 0)
    )
    if msg.get('aggregation_period'):
        msg['args'] = [msg.pop('args')]

    if msg.get('aggregation_pipeline'):
        msg['aggregation_pipeline'] = json.dumps(msg['aggregation_pipeline'])

    db.queue().counters.event.find_one_and_update(
        {'_id': msg.get('event')},
        {'$inc': {'c': 1}},
        upsert=True
    )
    db.queue().messages.insert_one(msg)
    return msg


def handle_message(msg):
    """ Обработка уведомления.
        Добавляет новое уведомление или присоединяет к уже имеющемуся.
    """
    msg_id = None
    if msg.get('aggregation_period'):
        # Задан период агрегации, пробуем группировать
        group = try_to_group(msg)
        if group:
            msg_id = "{}:{}".format(group['_id'], len(group['args']))

    if not msg_id:
        # Группировка не произошла - добавляем новое уведомление
        msg_id = add_to_queue(msg)['_id']

    return msg_id


class MessagesHandler(RequestHandler):
    def get(self, msg_id):
        """ Запрос статуса отправки сообщения
        """
        if ':' in msg_id:
            msg_id = msg_id.rpartition(':')[0]

        msg = db.queue().messages.find_one({"_id": bson.ObjectId(msg_id)})
        if msg is None:
            self.write(APIError(Codes.Not_Found))
            return

        msg['queuing_time'] = str(msg.get('queuing_time', ''))
        msg['processed_at'] = str(msg.get('processed_at', ''))
        msg['process_after'] = str(msg.get('process_after', ''))

        fields_of_interest = [
            "queuing_time",  # * Время добавления сообщения в очередь
            "processed_at",  # * Время обработки
            "process_duration",  # * Время обработки
            "process_after",  # Обработка начнётся не раньше этого момента
            "org",  # * Организация
            "status",  # * Статус
            "recipients",  # * Получатель
            "account_id",  # * Информация об использованном для отправки аккаунте,
                           # если попытка отправки уже произошла
            "error_msg",  # * Информация об ошибке, если есть.
            "error_hint",  # * Предполагаемая причина ошибки
        ]

        msg_info = {k: msg.get(k) for k in fields_of_interest}
        msg_info["id"] = str(msg['_id'])

        self.write(APIResult(msg_info))

    def post(self):
        """ Постановка сообщения в очередь по HTTP интерфейсу.
        """
        try:
            msg = json.loads(self.request.body)
        except Exception as e:
            self.set_status(400)
            logging.warning(u'Malformed json. {}'.format(e.message))
            logging.warning(u"Body:\n{}".format(self.request.body))
            self.write(
                APIError(
                    Codes.Request_Bad,
                    u'Malformed json. {}'.format(e.message)
                )
            )
            return

        try:
            validate(msg)
        except APIException as e:
            self.set_status(400)
            self.write(e.to_error())
            return

        try:
            msg_id = handle_message(msg)
        except:
            logging.error(
                "Malformed message: {}",
                dict_tools.pretty(msg),
                exc_info=True
            )
            self.set_status(400)
            self.write(APIError(Codes.Request_Bad, u'Malformed message.'))
        else:
            self.write(APIResult({'id': str(msg_id)}))
            logging.debug(
                "Message for: {}. Event: {}. Id: {}.".format(
                    msg.get('recipients'), msg.get('event'), msg_id
                )
            )


def start_server():
    listen_signals()

    handlers = [
        (r'/messages', MessagesHandler),
        (r'/messages/([a-z0-9:]+)', MessagesHandler),
        status_route,
    ]

    port = settings.get('port')
    logging.info('Starting Notifier Queue on port: {}.', port)

    application = tornado.web.Application(handlers)
    application.listen(port)
    try:
        tornado.ioloop.IOLoop.instance().start()
    except KeyboardInterrupt:
        logging.info('Stopping: Interrupted')


def create_indices():
    """ Обеспечить наличие индексов.
    """
    ttl = settings.get('message_ttl', 604800)
    db.queue().messages.create_index([('status', pymongo.ASCENDING)])
    db.queue().messages.create_index(
        [('queuing_time', pymongo.ASCENDING)],
        expireAfterSeconds=ttl
    )


def run():
    init_service(ServiceType.HTTP)
    create_indices()
    start_server()
