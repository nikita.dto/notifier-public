#!/usr/bin/env python
# coding: utf-8


from . import queue


def main():
    queue.run()


if __name__ == '__main__':
    main()
