# coding utf-8

name = 'notifier-queue'

maintainer = 'Nikita Vlaznev <nikita@ivideon.ru>'

version = '1.6.2'

description = """Ivideon Notifier Queue - message queue filling server."""

log_aggregation = "default"
