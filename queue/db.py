# coding: utf-8

""" Модуль подключений к базам данных
"""

from pymongo.read_preferences import ReadPreference

from ivideon.utils.mcache import run_once
from ivideon.nydus import Canal


@run_once
def queue():
    """ Подключение к базе с очередью.
    """
    return Canal(
        'notifier',
        rs_read_preference=ReadPreference.PRIMARY
    ).notifier
